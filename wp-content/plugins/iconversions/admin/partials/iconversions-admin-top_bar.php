<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       cg@prpwebs.com
 * @since      1.0.0
 *
 * @package    Iconversions
 * @subpackage Iconversions/admin/partials
 */
if(isset($_POST['topbarsave'])){
	var_dump($_POST);
}
?>
<div class="wrapper topbar">
	
	<form method="post">
	    <h1 class="topheader">Top Bar</h1>
	    <p>Top Bar will display top of the every page of the website.</p>
	    <hr/>

	    <label for="background"><b>Background Color</b></label>
	    <input type="color" name="backcolor" value="#ff0000" class="color-input">

	    <label for="color"><b>Text Color</b></label>
	    <input type="color" name="forecolor" value="#ffff26" class="color-input">

	    <label for="msg"><b>Message<span class="required">*</span></b></label>
	    <textarea rows="5" cols="20" class="form-input" required></textarea>
	    
	    <label for="link"><b>Link</b></label>
	    <input type="text" name="link" class="form-input">

	    <button type="submit" class="topbarsave" name="topbarsave">Save</button>
	</form>
</div>