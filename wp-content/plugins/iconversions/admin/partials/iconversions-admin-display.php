<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       cg@prpwebs.com
 * @since      1.0.0
 *
 * @package    Iconversions
 * @subpackage Iconversions/admin/partials
 */
?>

<?php
 
  if( isset( $_GET[ 'tab' ] ) ) {  
      $active_tab = $_GET[ 'tab' ];  
  } else {
      $active_tab = 'tab1';
  }
       
?>
<h2 class="nav-tab-wrapper">
    <a href="?page=iconversions&tab=tab1" class="nav-tab <?php echo $active_tab == 'tab1' ? 'nav-tab-active' : ''; ?>">Top Bar</a>
    <a href="?page=iconversions&tab=tab2" class="nav-tab <?php echo $active_tab == 'tab2' ? 'nav-tab-active' : ''; ?>">Popup</a>
    <a href="?page=iconversions&tab=tab3" class="nav-tab <?php echo $active_tab == 'tab3' ? 'nav-tab-active' : ''; ?>">Slider</a>
    <a href="?page=iconversions&tab=tab4" class="nav-tab <?php echo $active_tab == 'tab4' ? 'nav-tab-active' : ''; ?>">Product</a>
    <a href="?page=iconversions&tab=tab5" class="nav-tab <?php echo $active_tab == 'tab5' ? 'nav-tab-active' : ''; ?>">Cart</a>
</h2>

<?php
if( $active_tab == 'tab1' ) {
  include_once 'iconversions-admin-top_bar.php';
}

if( $active_tab == 'tab2' ) {
	echo 'Popup';
} 

if( $active_tab == 'tab3' ) {
	echo 'Slider';
} 

if( $active_tab == 'tab4' ) {
	echo 'Product';
} 

if( $active_tab == 'tab5' ) {
	echo 'Cart';
} 
 ?> 




