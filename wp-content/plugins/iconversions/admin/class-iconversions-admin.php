<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       cg@prpwebs.com
 * @since      1.0.0
 *
 * @package    Iconversions
 * @subpackage Iconversions/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Iconversions
 * @subpackage Iconversions/admin
 * @author     prpwebs <cg@prpwebs.com>
 */
class Iconversions_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Iconversions_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Iconversions_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/iconversions-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Iconversions_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Iconversions_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/iconversions-admin.js', array( 'jquery' ), $this->version, false );

	}

	/*add admin menu*/
	public function iconversions_admin_menu()
	{
		add_menu_page(
			'iconversions', //$page title
			 'i-conversions',// $menu title
			 'manage_options', //$capability
			 'iconversions', //$menu slug
			 array($this, 'iconversion_settings'), //$fuction
			 'dashicons-sort', // icon url
			 6
		);
		// This is the hidden page
	     /*add_submenu_page(
	      'iconversions', 
	      'setting',
	      'Settings', 
	      'manage_options', 
	      'iconversions', 
	      array($this, 'iconversions')
	     );*/
	}

	public function iconversion_settings(){
		include_once 'partials/iconversions-admin-display.php';
	}	
	
}
