<?php

/**
 * Fired during plugin activation
 *
 * @link       cg@prpwebs.com
 * @since      1.0.0
 *
 * @package    Iconversions
 * @subpackage Iconversions/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Iconversions
 * @subpackage Iconversions/includes
 * @author     prpwebs <cg@prpwebs.com>
 */
class Iconversions_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
