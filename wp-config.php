<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'iconversions' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'lH=+,w<Dfpv;y]|O*KD*f2ZVaj:F$>=)yRL(M.a2u/&q=ogPM.{Yxlqf*Z1JCUFN' );
define( 'SECURE_AUTH_KEY',  '(].4qzlR}GnObXL{<QD,xirVH7hbXX#[$RyDsJHF|~9L~R?-VIoKM7}%h *Yo)b7' );
define( 'LOGGED_IN_KEY',    ' q#o2+D/5T%5h`ZTY~u{gAj`pw>u.k>&z~wn_{PJzA>GvTxQpM3 }u$=2^+yj5v)' );
define( 'NONCE_KEY',        'I Cx]dR++<KsFj(O#_s]/}/&WeshO=uGe=i8_(lVTCME~N-S(VzhyvtG2EY=f-/{' );
define( 'AUTH_SALT',        '@P@rt(qL-`U)dLQ H/,}./YE@lWyR*1*XI[u.KDr}g/lZnyR2I.{*nHQ@CFOe0N-' );
define( 'SECURE_AUTH_SALT', '$A?*4YT@n}[zCx5XH^#HJ5N%2e_}ENBElSpSuZ&*xM5Dz9m&u5QFHvOOU$D|JcXh' );
define( 'LOGGED_IN_SALT',   '.aMHXLc]X^zn=5vTiFbksFo>uD3>F`KdUtX}}wqC[KSU%8n*BCMD9G?$q$>8w]ug' );
define( 'NONCE_SALT',       '@6M(3YF:q!C!)fJo&4.Pxe/_{.iFM(D-2W,h4NCjN2D041x1<l~$)b$w?_Zt;f5]' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
